# Explore Syslog

Added a Makefile to simply running this service and a Dockerfile to build this as a service.

```bash
docker build . -t explore-syslog:0.1
podman build . -t explore-syslog:0.1
docker run -it --rm -v $(pwd)/:/src/ -p 8514:8514 explore-syslog:0.1
docker run -it --rm -v $(pwd)/:/src/ -p 8514:8514 explore-syslog:0.1
podman run -it --rm -v $(pwd)/:/src/ -p 8514:8514 explore-syslog:0.1

# Run as deamon
docker run -d --name explore_syslog -v $(pwd)/:/src/ -p 8514:8514 explore-syslog:0.1
podman run -d --name explore_syslog --restart=always -v $(pwd)/:/src/ -p 8514:8514 explore-syslog:0.1
```

```shell
poetry run python syslog_server.py

poetry run python syslog_client.py -a 127.0.0.1 -p 8514 -m "hello"
```