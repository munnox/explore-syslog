#!/usr/bin/env python

##  Small Syslog inspired by https://gist.github.com/marcelom/4218010
##
##  RFC 5424 -  https://datatracker.ietf.org/doc/html/rfc5424
##  Syslog parser - https://github.com/EasyPost/syslog-rfc5424-parser/blob/master/README.md


LOG_FILE = "youlogfile.log"

import os
import logging
import socketserver
import re
import click
import json
import sys
import math
from syslog_rfc5424_parser import SyslogMessage, ParseError
from logging_handlers import setup_opensearch_handler
from dotenv import load_dotenv

logger = logging.getLogger(__file__)

load_dotenv()

facilities = {
    0: "Kernel", #"kernel messages",
    1: "User-Level", #"user-level messages",
    2: "Mail", #"mail system",
    3: "System", #"system daemons",
    4: "Security", #"security/authorization messages",
    5: "Internal", #"messages generated internally by syslogd",
    6: "Line", #"line printer subsystem",
    7: "Network", #"network news subsystem",
    8: "UUCP", #"UUCP subsystem",
    9: "Clock", # "clock daemon",
    10: "Security", #"security/authorization messages",
    11: "FTP", #"FTP daemon",
    12: "NTP", #"NTP subsystem",
    13: "log audit",
    14: "log alert",
    15: "clock daemon",
    16: "Local0", #"local use 0 (local0)",
    17: "Local1", #"local use 1 (local1)",
    18: "Local2", #"local use 2 (local2)",
    19: "Local3", #"local use 3 (local3)",
    20: "Local4", #"local use 4 (local4)",
    21: "Local5", #"local use 5; (local5)",
    22: "Local6", #"local use 6 (local6)",
    23: "Local7", #"local use 7 (local7)",
}

levels = {
    0: "EMERGENCY", #Emergency system is unusable",
    1: "ALERT", #Alert: action must be taken immediately",
    2: "CRITICAL", #Critical: critical conditions",
    3: "ERROR", #Error: error conditions",
    4: "WARNING", #Warning: warning conditions",
    5: "NOTICE", #Notice: normal but significant condition",
    6: "INFO" ,# Informational: informational messages",
    7: "DEBUG", #"Debug: debug-level messages",
}

# Basic Syslog RFC-5424 message parser
pattern1 = r"^\<(?P<priority>\d+)\>(?P<version>\d) (?P<timestamp>\d{4}-\d\d-\d\dT\d\d:\d\d:\d\d\.\d{6}[-+]\d\d:\d\d) (?P<hostname>.*?) (?P<appname>.*?) (?P<processid>.*?) (?P<msgid>.*?) (?P<structured>.*?) (?P<content>.*)$"
# Old style Syslog Parser BSD RFC-3164 to try to "fix" the date
pattern2 = r"^\<(?P<priority>\d+)\>(?P<timestamp>\w{3}[ ]+\d{1,2} \d{1,2}:\d{1,2}:\d{1,2}) (?P<hostname>[\w \-+.]+?) (?P<appname>(?:[\w\-+.,\[\]\(\)]+: )*)(?P<content>.*)$"
# IPv4, IPv6 and MAC regex pattern to search for interesting elements in a message.
pattern3 = r"(?P<ipv4>(?:[0-9]{1,3}\.{1}){3}(?:[0-9]{1,3}){1})|(?P<ipv6>(?:[A-Fa-f0-9]{4}:{1,2})(?:[A-Fa-f0-9]{1,4}:{1,2}?){0,3}(?:[A-Fa-f0-9]{1,4}){1})|(?P<mac>(?:[A-Fa-f0-9]{1,2}:){5}(?:[A-Fa-f0-9]{1,2}){1})"

class SyslogUDPHandler(socketserver.BaseRequestHandler):
    def handle(self):
        data = bytes.decode(self.request[0].strip(b" \n\x00"))
        socket = self.request[1]
        # print(f"Socket: {socket}")
        # print(f"Address:{self.client_address[0]}")
        # print(f"Log: {repr(str(data))} - {list(matches)}")
        message = data  # .decode('utf-8')
        msg_dict = {
            'raw_msg': message,
            # 'match_length': len(list(matches2)),
        }
        msg_dict = parse_msg(message, msg_dict)
        try:
            parsed_message = SyslogMessage.parse(message)
            # print(message.msg)
            msg_dict.update(parsed_message.as_dict())
            msg_dict['raw_msg'] = msg_dict['msg']
            del msg_dict['msg']
                
        except ParseError as e:
            pass
            # if matches2 is not None:
                
            #     logger.info(f"{message}", extra=msg_dict)
            # else:
            #     logger.error(f"Parse error and match2 None: {e}")
        logger.info(message, extra=msg_dict)
        # logging.info(str(data))
        # raise Exception("bye")

def parse_msg(msg, msg_dict):

    matches1 = re.finditer(pattern1, msg, re.M)
    matches2 = re.finditer(pattern2, msg, re.M)
    matches3 = re.finditer(pattern3, msg, re.M)
    for m in matches1:
        m1_groups = m.groupdict()
        msg_dict['m1_groups'] = m1_groups
    
    # for m in matches2:
    #     m2_groups = m.groupdict()
    #     msg_dict['m2_groups'] = m2_groups

    for m in matches2:
        m2_groups = m.groupdict()
        # logger.info(str(m))
        msg_dict['m2_groups'] = m2_groups
        msg_dict['m2_groups']['priority'] = int(msg_dict['m2_groups']['priority'])
        msg_dict['m2_groups']['facility'] = facilities[math.floor(msg_dict['m2_groups']['priority'] / 8)]
        msg_dict['m2_groups']['level'] = msg_dict['m2_groups']['priority'] % 8
        msg_dict['m2_groups']['levelname'] = levels[msg_dict['m2_groups']['level']]

    for m in matches3:
        m3_groups = m.groupdict()
        if 'm3_groups' in msg_dict:
            msg_dict['m3_groups'].append(m3_groups)
        else:
            msg_dict['m3_groups'] = [ m3_groups ]
        
    
    if 'm3_groups' in msg_dict:
        new_dict = {
            'ipv4': [ ele['ipv4'] for ele in msg_dict['m3_groups'] if ele['ipv4'] is not None],
            'ipv6': [ ele['ipv6'] for ele in msg_dict['m3_groups'] if ele['ipv6'] is not None],
            'mac': [ ele['mac'] for ele in msg_dict['m3_groups'] if ele['mac'] is not None],
        }
        msg_dict['m3_groups'] = new_dict
    return msg_dict

@click.command()
@click.option(
    "--address", "-a", default="0.0.0.0", help="The syslog message bind address"
)
@click.option(
    "--port", "-p", type=int, default=8514, help="The syslog message bind port"
)
@click.option("--logfile", default="default.log", help="Log file path.")
def server(address, port, logfile):
    # logging.basicConfig(
    #     level=logging.INFO,
    #     format="%(message)s",
    #     datefmt="",
    #     filename=logfile,
    #     filemode="a",
    # )

    # logging.basicConfig(
    #     # format="%(asctime)s %(levelname)s:%(name)s: %(message)s",
    #     format="%(message)s",
    #     level=logging.ERROR,
    #     datefmt="%H:%M:%S",
    #     stream=sys.stderr,
    # )
    uri = os.getenv("OPENSEARCH_LOGGING_URI", None)
    logging_index = os.getenv("OPENSEARCH_LOGGING_INDEX", "logging_syslog_index")
    opensearch_handler = setup_opensearch_handler(uri, logging_index)
    logger.setLevel(logging.INFO)
    logger.addHandler(opensearch_handler)
    try:
        server = socketserver.UDPServer(
            (address, port), 
            SyslogUDPHandler
        )
        msg = [
            f"Syslog server starting bound on: {address}:{port}",
            f"Opensearch starting bound on: {uri} index: {logging_index}"
        ]
        click.echo("\n".join(msg))
        logger.info("\n".join(msg))
        server.serve_forever()  # poll_interval=0.5)
    except (IOError, SystemExit):
        raise
    except KeyboardInterrupt:
        msg = "Crtl+C Pressed. Shutting down."
        click.echo(msg)
        logger.info(msg)


if __name__ == "__main__":
    server(auto_envvar_prefix="SYSLOG")
