import click
from syslog_server import server
from syslog_client import client

main = click.Group()
main.add_command(server, "server")
main.add_command(client, "client")

if __name__ == "__main__":
    main(auto_envvar_prefix="SYSLOG")