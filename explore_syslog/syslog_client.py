#!/usr/bin/env python

import logging
import logging.handlers
import click


@click.command()
@click.option(
    "--address", "-a", default="127.0.0.1", help="The syslog message recipient address"
)
@click.option(
    "--port", "-p", type=int, default=8514, help="The syslog message recipient port"
)
@click.option("--level", "-l", default="DEBUG", help="The syslog message log level")
@click.option("--message", "-m", required=True, help="The syslog message")
@click.option("--replay", "-r", default="replay.txt", help="A log to replay")
def client(address, port, level, message, replay):
    "A syslog message generator"
    syslogger = logging.getLogger("SyslogLogger")
    level = string_to_level(level)
    syslogger.setLevel(level)
    handler = logging.handlers.SysLogHandler(address=(address, port), facility=19)
    # handler.setLevel(level)
    syslogger.addHandler(handler)
    print(f"Sending {handler} {address}, {port}: {repr(level)} {level} - {message}")
    syslogger.log(level, message)

    # Replay old logs for testing from a file
    with open(replay) as fh:
        replay = fh.read()
    lines = replay.split("\n")
    for line in lines:
        # print(f"Line: {repr(line)}, Syslog matches: {list(matches)}")
        first_gt = line.find(">", 0, 10)
        if first_gt >= 0:
            log_with_pri_removed = line[first_gt + 1 :]
        else:
            log_with_pri_removed = line
        syslogger.log(level, log_with_pri_removed)

    syslogger.error("test error level")


def string_to_level(log_level):
    """Convert a commandline string to a proper log level
    @param string log_level     command line log level argument
    @return logging.LEVEL       the logging.LEVEL object to return
    """
    if log_level == "CRITICAL":
        return logging.CRITICAL
    if log_level == "ERROR":
        return logging.ERROR
    if log_level == "WARNING":
        return logging.WARNING
    if log_level == "INFO":
        return logging.INFO
    if log_level == "DEBUG":
        return logging.DEBUG
    return logging.NOTSET


if __name__ == "__main__":
    client(auto_envvar_prefix="SYSLOG")
