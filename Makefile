CONTAINER = podman
CONTAINER_IMAGE = explore-syslog:0.1
CONTAINER_NAME = explore_syslog


build:
	$(CONTAINER) build . -t $(CONTAINER_IMAGE)

shell:
	$(CONTAINER) run -it --rm --name $(CONTAINER_NAME)_shell -v $(shell pwd)/:/src/ -p 8514:8514/udp $(CONTAINER_IMAGE)

deamon:
	$(CONTAINER) run -d --name $(CONTAINER_NAME) --restart=always -p 8514:8514/udp $(CONTAINER_IMAGE)

clean:
	$(CONTAINER) rm -f $(CONTAINER_NAME)